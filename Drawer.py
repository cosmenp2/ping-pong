# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import pygame

from Ball import Ball
from Manager import Manager
from Player import Player
from Goal import Goal
from Text import Text


class Drawer:

    def __init__(self, settings, screen):
        """
        Create necessary objects for all players
        :param settings: Necessary for give properties to objects
        :param screen: Necessary for draw into screen
        """
        self.settings = settings
        self.screen = screen
        self.screen_rect = self.screen.get_rect()
        self.players = []
        self.goals = []
        self.scores = []
        self.tutorials = []
        self.started = False
        self.show_tutorial = True
        self.fade = False
        self.color = 255
        self.scoreboard = Text(self.screen, self.settings, 100, (50, 50, 50), "")

        self.players.append(Player(self.screen, self.settings.distance,
                                   self.screen_rect.centery - self.settings.players_height / 2,
                                   self.settings.players_width, self.settings.players_height,
                                   self.settings.players_speed))
        if self.settings.IA:
            self.players.append(Player(self.screen,
                                       self.screen_rect.width - (self.settings.distance + self.settings.players_width),
                                       self.screen_rect.centery - self.settings.players_height / 2,
                                       self.settings.players_width, self.settings.players_height,
                                       self.settings.IA_speed))
            self.tutorials.append(Text(self.screen, self.settings, 50, (self.color, self.color, self.color),
                                       "Up Down", "left"))
        else:
            self.players.append(Player(self.screen,
                                       self.screen_rect.width - (self.settings.distance + self.settings.players_width),
                                       self.screen_rect.centery - self.settings.players_height / 2,
                                       self.settings.players_width, self.settings.players_height,
                                       self.settings.players_speed))
            self.tutorials.append(Text(self.screen, self.settings, 50, (self.color, self.color, self.color), "Q A",
                                       "left"))
            self.tutorials.append(Text(self.screen, self.settings, 50, (self.color, self.color, self.color),
                                       "Up Down", "right"))

        self.scores.append(0)
        self.goals.append(Goal(self.screen, 0, 0, 1, self.screen_rect.height))
        self.scores.append(0)
        self.goals.append(Goal(self.screen, self.screen_rect.width, 0, 1, self.screen_rect.height))

        if self.settings.players > 2:
            self.players.append(Player(screen, self.screen_rect.centerx - self.settings.players_height / 2,
                                       self.settings.distance, self.settings.players_height,
                                       self.settings.players_width, self.settings.players_speed))
            self.goals.append(Goal(self.screen, 0, 0, self.screen_rect.width, 1))
            self.scores.append(0)
            self.tutorials.append(Text(self.screen, self.settings, 50, (self.color, self.color, self.color), "R T",
                                       "center", "top"))

        if self.settings.players > 3:
            self.players.append(Player(screen, self.screen_rect.centerx - self.settings.players_height / 2,
                                       self.screen_rect.height - (self.settings.distance + self.settings.players_width),
                                       self.settings.players_height,
                                       self.settings.players_width, self.settings.players_speed))
            self.goals.append(Goal(self.screen, 0, self.screen_rect.height, self.screen_rect.width, 1))
            self.scores.append(0)
            self.tutorials.append(Text(self.screen, self.settings, 50, (self.color, self.color, self.color), "K L",
                                       "center", "bottom"))

        if self.settings.IA:
            self.scoreboard.update_text("J1:CPU")
        else:
            text = "J1"
            for i in range(1, len(self.players)):
                text += ":J" + str(i + 1)
            self.scoreboard.update_text(text)

        self.ball = Ball(self.screen, self.screen_rect.centerx - self.settings.ball_size / 2,
                         self.screen_rect.centery - self.settings.ball_size / 2, self.settings.ball_size,
                         self.settings.ball_size, self.settings.initial_ball_speed)
        self.manager = Manager(self.settings, self.players, self.goals, self.ball, self.screen)

    def draw(self):
        """
        Draw all the objects into screen. Players, ball, tutorials and scoreboard
        """
        if not self.started and self.manager.start:
            self.started = True
            self.update_scoreboard()
            self.fade = True
        if self.manager.end:
            self.restart()
        self.manager.check_events()
        self.manager.update()
        self.screen.fill((0, 0, 0))
        for player in self.players:
            player.draw()

        if self.fade:
            if self.color == 0:
                self.fade = False
                self.show_tutorial = False
            else:
                self.color -= 1
                for tutorial in self.tutorials:
                    tutorial.color = (self.color, self.color, self.color)

        if self.show_tutorial:
            for tutorial in self.tutorials:
                tutorial.show_text()
        self.scoreboard.show_text()
        self.ball.draw()
        pygame.display.flip()

    def restart(self):
        """
        Relocate all the objects in their original positions and set the variables to start again the game
        """
        self.players[0].x = self.settings.distance
        self.players[0].y = self.screen_rect.centery - self.settings.players_height / 2
        self.players[1].x = self.screen_rect.width - (self.settings.distance + self.settings.players_width)
        self.players[1].y = self.screen_rect.centery - self.settings.players_height / 2

        if self.settings.players > 2:
            self.players[2].x = self.screen_rect.centerx - self.settings.players_height / 2
            self.players[2].y = self.settings.distance
        if self.settings.players > 3:
            self.players[3].x = self.screen_rect.centerx - self.settings.players_height / 2
            self.players[3].y = self.screen_rect.height - (self.settings.distance + self.settings.players_width)

        self.ball.x = self.screen_rect.centerx - self.settings.ball_size / 2
        self.ball.y = self.screen_rect.centery - self.settings.ball_size / 2
        self.manager.ball_directionx = 0
        self.manager.ball_directiony = 0
        self.manager.start = False
        self.manager.end = False
        if self.settings.IA:
            self.manager.IA.calculated = False
        self.ball.speed = self.settings.initial_ball_speed
        self.update_scoreboard()
        self.manager.last = 0

    def update_scoreboard(self):
        """
        Update the screen scoreboard
        """
        text = ""
        for i in range(0, len(self.players)):
            if i == 0:
                text += str(self.players[i].score)
            else:
                text += ":" + str(self.players[i].score)
        self.scoreboard.update_text(text)
