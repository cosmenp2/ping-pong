# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import os

import pygame


class Text:

    def __init__(self, screen, settings, size, color, text, h_alignment="center", v_alignment="center"):
        """
        This is to show scoreboard and tutorial into screen
        :param screen: Necessary to show into screen
        :param settings: Necessary to calculate the positions
        :param size: Size text
        :param color: Color text
        :param text: The phrase to show
        :param h_alignment: Horizontal alignment
        :param v_alignment: Vertical alignment
        """
        self.screen = screen
        self.screen_rect = self.screen.get_rect()
        self.settings = settings
        self.color = color
        self.text = text
        self.font = pygame.font.Font(os.path.join("Resources", "BAUHS93.TTF"), size)

        self.label = self.font.render(self.text, True, self.color)
        self.label_rect = self.label.get_rect()
        self.h_alignment = h_alignment
        self.v_alignment = v_alignment
        self.x = 0
        self.y = 0

        self.calculate_position()

    def update_text(self, text):
        """
        Update the text and update the new positions
        :param text: New phrase to show
        """
        self.text = text
        self.label = self.font.render(self.text, True, self.color)
        self.label_rect = self.label.get_rect()
        self.calculate_position()

    def calculate_position(self):
        """
        Calculate the positions of the label with the text
        """
        if self.h_alignment == "left":
            self.x = self.settings.distance * 2 + self.settings.players_width
            self.y = self.settings.screen_height / 2 - self.label_rect.height / 2
        elif self.h_alignment == "right":
            self.x = self.settings.screen_width - self.settings.players_width - self.settings.distance * 2 - \
                     self.label_rect.width
            self.y = self.settings.screen_height / 2 - self.label_rect.height / 2
        else:
            if self.v_alignment == "top":
                self.y = self.settings.distance * 2 + self.settings.players_height
                self.x = self.settings.screen_width / 2 - self.label_rect.width / 2
            elif self.v_alignment == "bottom":
                self.y = self.settings.screen_height - self.settings.players_height - self.settings.distance * 2 \
                         - self.label_rect.height
                self.x = self.settings.screen_width / 2 - self.label_rect.width / 2
            else:
                self.x = self.settings.screen_width / 2 - self.label_rect.width / 2
                self.y = self.settings.screen_height / 2 - self.label_rect.height / 2

    def show_text(self):
        """
        Show the text into screen
        Use label instead of self.label because if the color change, the change will take effect
        """
        label = self.font.render(self.text, True, self.color)
        self.screen.blit(label, (self.x, self.y))
