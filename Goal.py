# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import pygame


class Goal:
    def __init__(self, screen, posx, posy, width, height):
        """
        Create the goal
        :param screen: Necessary to draw
        :param posx: X position
        :param posy: Y position
        :param width: Goal width
        :param height: Goal height

        """
        super(Goal, self).__init__()

        self.screen = screen
        self.rect = pygame.Rect(0, 0, width, height)
        self.rect.x = posx
        self.rect.y = posy
