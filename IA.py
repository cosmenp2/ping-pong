# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.


class IA:
    def __init__(self, screen, ball, player):
        """
        Create the IA for the player 2 only.
        :param screen: Necessary for calculate the movements
        :param ball: Necessary for calculate the movements
        :param player: Necessary for calculate the movements and move the player
        """
        self.screen = screen
        self.screen_rect = self.screen.get_rect()
        self.ball = ball
        self.player = player
        self.calculated = False
        self.positiony = 0
        self.count = 0

    def calculate_position(self, ball_directionx, ball_directiony):
        """
        Calculate the position where the ball will be
        :param ball_directionx: Necessary to calculate the space between player and ball
        :param ball_directiony: Necessary to calculate if the ball go up or down or nothing.
        """
        if self.calculated:
            self.move()
        elif ball_directionx == -1:
            self.move()
        elif ball_directionx == 1:
            if ball_directiony == 1:
                cicles = 0
                space = abs(self.ball.x + self.ball.rect.width - self.player.x) - self.ball.y
                while space >= self.screen_rect.height:
                    cicles += 1
                    space -= self.screen_rect.height
                if cicles % 2 == 0:
                    self.positiony = space - self.player.rect.height / 2
                else:
                    self.positiony = self.screen_rect.height - space - self.player.rect.height / 2
            elif ball_directiony == -1:
                cicles = 0
                space = abs(self.ball.x + self.ball.rect.width - self.player.x) - self.screen_rect.height + self.ball.y
                while space >= self.screen_rect.height:
                    cicles += 1
                    space -= self.screen_rect.height
                if cicles % 2 != 0:
                    self.positiony = space - self.player.rect.height / 2
                else:
                    self.positiony = self.screen_rect.height - space - self.player.rect.height / 2
            else:
                self.positiony = self.ball.y - self.player.rect.height / 2
                if self.count >= 2:
                    self.positiony = self.ball.y - self.player.rect.height / 5
                    self.count = 0
                self.count += 1

            self.calculated = True

    def move(self):
        """
        Move the player according with the variable position y and calculated
        """
        if self.calculated:
            if self.positiony - 2 < self.player.y < self.positiony + 2:
                pass  # To avoid doddering
            elif self.player.y < self.positiony:
                self.player.update(-1, False)
            elif self.player.y > self.positiony:
                self.player.update(1, False)
        else:
            if self.screen_rect.centery - self.player.rect.height / 2 - 2 < self.player.y < \
                    self.screen_rect.centery - self.player.rect.height / 2 + 2:
                pass  # To avoid doddering
            elif self.player.y < self.screen_rect.centery - self.player.rect.height / 2:
                self.player.update(-1, False)
            elif self.player.y > self.screen_rect.centery - self.player.rect.height / 2:
                self.player.update(1, False)
