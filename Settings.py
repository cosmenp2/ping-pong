# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.


class Settings:

    def __init__(self):
        """
        Settings
        """
        self.players = 2  # 2 to 4. If you want to play alone set IA = True
        self.screen_width = 950
        self.screen_height = 700
        self.IA = True
        self.distance = 20
        self.players_width = 10
        self.players_height = 100
        self.players_speed = 3
        self.IA_speed = 1
        self.initial_ball_speed = 0.7
        self.ball_size = 15
