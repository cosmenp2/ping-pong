#!/bin/python
# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import pygame

from Drawer import Drawer
from Settings import Settings

settings = Settings()
pygame.init()
pygame.font.init()
screen = pygame.display.set_mode((settings.screen_width, settings.screen_height))
drawer = Drawer(settings, screen)
while True:
    drawer.draw()
