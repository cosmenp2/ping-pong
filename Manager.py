# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import sys

import pygame

from IA import IA


class Manager:

    def __init__(self, settings, players, goals, ball, screen):
        """
        Manage all the components of the game and establish the rules
        :param settings: Necessary to access for know if the IA have to use
        :param players: Necessary to manage them
        :param goals: Necessary for know when one player score
        :param ball: Necessary to manage the ball
        :param screen: Necessary for the IA
        """
        self.settings = settings
        self.players = players
        self.goals = goals
        self.ball = ball
        self.movements = []
        if self.settings.IA:
            self.IA = IA(screen, self.ball, self.players[1])
        for i in range(0, len(self.players)):
            self.movements.append(0)
        self.ball_directionx = 0
        self.ball_directiony = 0
        self.count = 0
        self.start = False
        self.end = False
        self.last = 0

    def check_events(self):
        """
        Check the keyboard events and change the movements variables
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if not self.start:
                        self.ball_directionx = 1
                        self.start = True
                if self.settings.IA:
                    if event.key == pygame.K_UP:
                        self.movements[0] += 1
                    if event.key == pygame.K_DOWN:
                        self.movements[0] -= 1
                if not self.settings.IA:
                    if event.key == pygame.K_q:
                        self.movements[0] += 1
                    if event.key == pygame.K_a:
                        self.movements[0] -= 1
                    if event.key == pygame.K_UP:
                        self.movements[1] += 1
                    if event.key == pygame.K_DOWN:
                        self.movements[1] -= 1
                if len(self.players) > 2:
                    if event.key == pygame.K_r:
                        self.movements[2] += 1
                    if event.key == pygame.K_t:
                        self.movements[2] -= 1
                if len(self.players) > 3:
                    if event.key == pygame.K_k:
                        self.movements[3] += 1
                    if event.key == pygame.K_l:
                        self.movements[3] -= 1
            if event.type == pygame.KEYUP:
                if self.settings.IA:
                    if event.key == pygame.K_UP:
                        self.movements[0] -= 1
                    if event.key == pygame.K_DOWN:
                        self.movements[0] += 1
                if not self.settings.IA:
                    if event.key == pygame.K_q:
                        self.movements[0] -= 1
                    if event.key == pygame.K_a:
                        self.movements[0] += 1
                    if event.key == pygame.K_UP:
                        self.movements[1] -= 1
                    if event.key == pygame.K_DOWN:
                        self.movements[1] += 1
                if len(self.players) > 2:
                    if event.key == pygame.K_r:
                        self.movements[2] -= 1
                    if event.key == pygame.K_t:
                        self.movements[2] += 1
                if len(self.players) > 3:
                    if event.key == pygame.K_k:
                        self.movements[3] -= 1
                    if event.key == pygame.K_l:
                        self.movements[3] += 1

    def update(self):
        """
        Update all the movements, call to the IA, check crashes and increase speed the ball
        """
        for i in range(0, len(self.movements)):
            if i < 2:
                self.players[i].update(self.movements[i], False)
            else:
                self.players[i].update(self.movements[i], True)

        if self.ball.y == 0:
            self.ball_directiony = -1
        elif self.ball.y == self.settings.screen_height - self.ball.rect.height:
            self.ball_directiony = 1
        self.ball.update(self.ball_directionx, self.ball_directiony)

        if self.ball_directionx == 1:
            self.check_crash(2)
        elif self.ball_directionx == -1:
            self.check_crash(1)
        if len(self.players) >= 3:
            if self.ball_directiony == 1:
                self.check_crash(3)
            if len(self.players) == 4 and self.ball_directiony == -1:
                self.check_crash(4)

        if self.settings.IA:
            self.IA.calculate_position(self.ball_directionx, self.ball_directiony)
        if self.count >= 4:
            self.ball.speed += 0.1
            self.count = 0

    def check_crash(self, player):
        """
        Check if the ball crash with one player or with the goals, increase points of the players
        if the IA is activated update the IA if the player 2 crash and update count for increase ball speed
        :param player: the player and goal to check
        """
        crash = -1
        # For players 1 and 2
        # Comparative in X axis

        if player == 1:

            if self.ball.x >= self.players[player - 1].x:
                if abs(self.ball.x - self.players[player - 1].x) <= self.players[player - 1].rect.width:
                    crash = player - 1
            else:
                if abs(self.ball.x - self.players[player - 1].x) <= self.ball.rect.width:
                    crash = player - 1
        elif player == 2:
            if self.ball.x <= self.players[player - 1].x:
                if abs(self.ball.x - self.players[player - 1].x) <= self.ball.rect.width:
                    crash = player - 1
            else:
                if abs(self.ball.x - self.players[player - 1].x) <= self.players[player - 1].rect.width:
                    crash = player - 1

        # Comparative in Y axis

        if crash != -1 and ((self.players[crash].y < self.ball.y <=
                             self.players[crash].y + self.players[crash].rect.height) or
                            (self.players[crash].y < self.ball.y + self.ball.rect.height <=
                             self.players[crash].y + self.players[crash].rect.height)):
            if crash == 0:
                self.ball_directionx = 1
                self.count += 1
                self.last = player
            elif crash == 1:
                self.ball_directionx = -1
                if self.settings.IA:
                    self.IA.calculated = False
                self.count += 1
                self.last = player

            center = self.ball.y + self.ball.rect.height / 2

            if self.players[crash].y - self.ball.rect.height / 2 <= center <= \
                    self.players[crash].y + self.players[crash].rect.height / 3:

                self.ball_directiony = 1
            elif self.players[crash].y + self.players[crash].rect.height * 2 / 3 < center <= \
                    self.players[crash].y + self.ball.rect.height / 2 + self.players[crash].rect.height:

                self.ball_directiony = -1

        elif (player == 1 and abs(self.ball.x - self.goals[player - 1].rect.x) <= 0) or player == 2 and \
                abs(self.ball.x + self.ball.rect.width - self.goals[player - 1].rect.x) <= 0:
            self.end = True
            if self.last > 0:
                self.players[self.last-1].score += 1
            elif len(self.players) == 2:
                self.players[0].score += 1

        # For players 3 and 4
        # Comparative in Y axis
        if player == 3:

            if self.ball.y >= self.players[player - 1].y:
                if abs(self.ball.y - self.players[player - 1].y) <= self.players[player - 1].rect.height:
                    crash = player - 1
            else:
                if abs(self.ball.y - self.players[player - 1].y) <= self.ball.rect.height:
                    crash = player - 1
        elif player == 4:
            if self.ball.y <= self.players[player - 1].y:
                if abs(self.ball.y - self.players[player - 1].y) <= self.ball.rect.height:
                    crash = player - 1
            else:
                if abs(self.ball.y - self.players[player - 1].y) <= self.players[player - 1].rect.height:
                    crash = player - 1

        # Comparative in X axis

        if crash > 1 and ((self.players[crash].x < self.ball.x <=
                           self.players[crash].x + self.players[crash].rect.width) or
                          (self.players[crash].x < self.ball.x + self.ball.rect.width <= self.players[crash].x +
                           self.players[crash].rect.width)):
            if crash == 2:
                self.ball_directiony = -1
                self.count += 1
                self.last = player
            elif crash == 3:
                self.ball_directiony = 1
                self.count += 1
                self.last = player

            center = self.ball.x + self.ball.rect.width / 2

            if self.players[crash].x - self.ball.rect.width / 2 <= center <= \
                    self.players[crash].x + self.players[crash].rect.width / 3:

                self.ball_directionx = -1
            elif self.players[crash].x + self.players[crash].rect.width * 2 / 3 < center <= \
                    self.players[crash].x + self.ball.rect.width / 2 + self.players[crash].rect.width:

                self.ball_directionx = 1

        elif (player == 3 and abs(self.ball.y - self.goals[player - 1].rect.y) <= 0) or player == 4 and \
                abs(self.ball.y + self.ball.rect.height - self.goals[player - 1].rect.y) <= 0:
            self.end = True
            if self.last > 0:
                self.players[self.last-1].score += 1
