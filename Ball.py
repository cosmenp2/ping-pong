# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import pygame


class Ball:
    def __init__(self, screen, posx, posy, width=5, height=5, speed=1, color=(255, 255, 255)):
        """
        Create the ball
        :param screen: Necessary to draw
        :param posx: Initial x position
        :param posy: Initial y position
        :param width: Ball width
        :param height: Ball height
        :param color: Ball color
        :param speedx: Ball speed x
        :param speedy: Ball speed y
        """
        super(Ball, self).__init__()

        self.screen = screen
        self.screen_rect = self.screen.get_rect()
        self.color = color
        self.rect = pygame.Rect(0, 0, width, height)
        self.rect.x = posx
        self.rect.y = posy
        self.y = self.rect.y
        self.x = self.rect.x
        self.speed = speed

    def draw(self):
        """
        Draw the object into the screen
        """
        pygame.draw.rect(self.screen, self.color, self.rect)

    def update(self, directionx, directiony):
        """
        Update the position of the ball
        :param directionx: 1 if go right, -1 if go left and 0 if not go.
        :param directiony: 1 if go up, -1 if go down and 0 if not go.
        """
        movex = 0
        self.y += self.speed * directiony * -1
        if self.y < 0:
            movex = self.speed - abs(self.y)
            self.y = 0
        elif self.y + self.rect.height > self.screen_rect.height:
            movex = self.speed - abs(self.y + self.rect.height - self.screen_rect.height)
            self.y = self.screen_rect.height - self.rect.height
        else:
            movex = self.speed
        self.rect.y = self.y

        self.rect.y = self.y

        if self.x < 0:
            self.x = 0
        elif self.x + self.rect.width >= self.screen_rect.width:
            self.x = self.screen_rect.width - self.rect.width
        else:
            self.x += movex * directionx

        self.rect.x = self.x
