# Copyright (C) 2019 Cosme Jose Nieto Perez <cosmenp@gmail.com>
#
# This file is part of Ping pong.
#
#     Ping pong is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     any later version.
#
#     Ping pong is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with Ping pong.  If not, see <https://www.gnu.org/licenses/>.

import pygame


class Player:
    def __init__(self, screen, posx, posy, width, height, speed=1, color=(255, 255, 255)):
        """
        Create the rectangle and the player score
        :param screen: Necessary to draw
        :param posx: Initial x position
        :param posy: Initial y position
        :param width: Rectangle width
        :param height: Rectangle height
        :param speed: Rectangle speed
        :param color: Rectangle color
        """
        super(Player, self).__init__()

        self.score = 0
        self.screen = screen
        self.screen_rect = self.screen.get_rect()
        self.color = color
        self.rect = pygame.Rect(0, 0, width, height)
        self.rect.x = posx
        self.rect.y = posy
        self.y = self.rect.y
        self.x = self.rect.x
        self.speed = speed

    def draw(self):
        """
        Draw the object into the screen
        """
        pygame.draw.rect(self.screen, self.color, self.rect)

    def update(self, direction, invert=False):
        """
        Update the position of the player
        :param direction: 1 if go up, -1 if go down and 0 if not go.
        :param invert: True if the player move horizontally, False if the player move vertically
        """
        if invert:
            self.x += self.speed * direction * -1
            if self.x < 0:
                self.x = 0
            elif self.x + self.rect.width > self.screen_rect.width:
                self.x = self.screen_rect.width - self.rect.width
            self.rect.x = self.x
        else:
            self.y += self.speed * direction * -1
            if self.y < 0:
                self.y = 0
            elif self.y + self.rect.height > self.screen_rect.height:
                self.y = self.screen_rect.height - self.rect.height
            self.rect.y = self.y
